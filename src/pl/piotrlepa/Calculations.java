package pl.piotrlepa;

import java.util.Arrays;

public class Calculations {
    static Point2D positionGeometricCenter(Point2D[] points) {
        int size = points.length;
        double x = Arrays.stream(points).mapToDouble(Point2D::getX).sum() / size;
        double y = Arrays.stream(points).mapToDouble(Point2D::getY).sum() / size;
        return new Point2D(x, y);
    }

    static Point2D positionCenterOfMass(MaterialPoint2D[] points) {
        double x = Arrays.stream(points).mapToDouble((p) -> p.getX() * p.getMass()).sum();
        double y = Arrays.stream(points).mapToDouble((p) -> p.getY() * p.getMass()).sum();
        int mass = Arrays.stream(points).mapToInt(MaterialPoint2D::getMass).sum();
        return new MaterialPoint2D(x / mass, y / mass, mass);
    }
}
