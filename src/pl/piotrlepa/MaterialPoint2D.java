package pl.piotrlepa;

public class MaterialPoint2D extends Point2D {
    private final int mass;

    public MaterialPoint2D(double x, double y, int mass) {
        super(x, y);
        this.mass = mass;
    }

    public int getMass() {
        return mass;
    }

    @Override
    public String toString() {
        return super.toString() + "MaterialPoint2D{" +
                "mass=" + mass +
                '}';
    }
}
